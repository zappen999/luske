# Luske
> Processing multiplexer/batcher

Inspired by [DataLoader](https://github.com/facebook/dataloader), but with time
based loading trigger.

Example usage:

```js
const Luske = require('luske');

function batchingFunc(args) {
    // Load up your data in your custom batching way. In this case, we are not
    // picky, we just return the arguments.
    return args;
}

const loader = new Luske(batchingFunc, {
    timeoutMs: 100,
    batchSize: 10,
});

const promise1 = loader.load(1); // Resolves to 1
const promise2 = loader.load(2); // Resolves to 2
// ...
```

## Reference

### Luske
```ts
Luske(
    batchingFunc: (loadCriterias: Array<any>) => Promise<Array<any>>,
    opts: Options,
): Luske
```

#### Options
```ts
interface Options {
    // How long to wait for next load call before loading (default 100ms).
    timeoutMs?: number
    // How many load requests to handle at a time (no limit by default).
    batchSize?: number
    // Don't reuse promises if the arguments is the same (default false).
    disableCache?: boolean
}
```

#### Luske#load
Initiates a load request with any load criteria.

```ts
load(loadCritera: any) => Promise<any>
```
