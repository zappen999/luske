const expect = require('chai').expect;
const sinon = require('sinon');

const Luske = require('./index');

describe('Multiplexer', function() {
    it('should operate in chunks defined by batchSize opt', async function() {
        const batchingFunc = sinon.stub().returnsArg(0);
        const l = new Luske(batchingFunc, { timoutMs: 20, batchSize: 2 });
        const promises = [];

        for (let i = 0; i < 7; i++) {
            promises.push(l.load(i));
        }

        await Promise.all(promises);
        expect(batchingFunc.callCount).to.equal(4);
    });

    it('should get same promise for identical loads', async function() {
        const batchingFunc = sinon.stub().returnsArg(0);
        const l = new Luske(batchingFunc, { timoutMs: 20 });

        const p1 = l.load('one');
        const p2 = l.load('one');

        const [v1, v2] = await Promise.all([p1, p2]);

        expect(p1 === p2).to.equal(true);
        expect(batchingFunc.firstCall.calledWith(['one']));
    });

    it(
        'should not drain at timeout if batchSize limit was last called',
        async function() {
            const batchingFunc = sinon.stub().returnsArg(0);
            const l = new Luske(batchingFunc, { timeoutMs: 20, batchSize: 2 });

            await Promise.all([
                l.load('one'),
                l.load('two'),
            ]);

            expect(batchingFunc.callCount).to.equal(1);
        }
    );

    it(
        'should not cache promises when disableCache is true',
        async function() {
            const batchingFunc = sinon.stub().returnsArg(0);
            const l = new Luske(batchingFunc, {
                timeoutMs: 20,
                disableCache: true,
            });

            const first = l.load('one');
            const second = l.load('one');

            expect(first === second).to.equal(false);
        },
    );

    it(
        'should allow the batcher to not return any values',
        async function() {
            const batchingFunc = sinon.stub();
            const l = new Luske(batchingFunc, {
                timeoutMs: 20,
                disableCache: true,
            });

            expect(() => l.load('one')).not.to.throw(Error);
        },
    );
});
