const uuid = require('uuid/v4');

// Not really hashing, but it's sufficient.
function hashObject(obj) {
    return JSON.stringify(obj);
}

class Luske {
    constructor(batchingFunc, opts = {}) {
        this.batchingFunc = batchingFunc;
        this.opts = Object.assign({
            timeoutMs: 100,
            batchSize: false,
            disableCache: false,
            disableEmptyBatcherValuesWarning: false,
        }, opts);

        this.timer = null;
        this.currentBatch = {};
        this.promiseCacheNamespace = uuid();
        this.promiseCache = {};
    }

    load(args) {
        this.postponeLoading();

        const key = this.getPromiseCacheKey(args);

        if (this.promiseCache[key]) {
            return this.promiseCache[key];
        }

        const promise = new Promise((resolve, reject) => {
            this.currentBatch[key] = {
                args: args,
                resolve: resolve,
                reject: reject,
            };
        });

        this.promiseCache[key] = promise;

        if (this.opts.batchSize &&
            Object.keys(this.currentBatch).length >= this.opts.batchSize
        ) {
            this.drainCurrentBatch();
            clearTimeout(this.timer);
        }

        return promise;
    }

    postponeLoading() {
        this.clearTimer();

        this.timer = setTimeout(
            () => this.drainCurrentBatch(),
            this.opts.timeoutMs,
        );
    }

    clearTimer() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    }

    getPromiseCacheKey(data) {
        let key = this.promiseCacheNamespace + ':' + hashObject(data);

        if (this.opts.disableCache) {
            key += ':' + uuid();
        }

        return key;
    }

    drainCurrentBatch() {
        const drainBatch = this.currentBatch;
        this.currentBatch = {};
        this.promiseCacheNamespace = uuid();
        this.clearTimer();

        const keys = Object.keys(drainBatch);
        const requestArgs = keys.map(key => drainBatch[key].args);

        if (keys.length === 0) {
            // This may be the result of a race condition between timeout and
            // batchSize. We don't want to call the batchingFunc with no
            // arguments.
            return;
        }

        Promise.resolve(this.batchingFunc(requestArgs))
            .then(arr => {
                let values = arr;

                if (typeof values === 'undefined') {
                    this.warnAboutEmptyBatcherValues();
                    values = [];
                }

                keys.forEach((key, i) => drainBatch[key].resolve(values[i]));
            })
            .catch(err => {
                keys.forEach(key => drainBatch[key].reject(err));
            });

        // Clear the promise cache for this batch
        keys.forEach(key => this.promiseCache[key] = undefined);
    }

    warnAboutEmptyBatcherValues() {
        if (this.opts.disableEmptyBatcherValuesWarning) {
            return;
        }

        console.log(
            'Luske WARN: Batching function returned no values. This may be a ' +
            'bug. If you don\'t care about the resolved values of the load() ' +
            'call, you can safely ignore this warning. To disable this ' +
            'warning, set disableEmptyBatcherValuesWarning to true.'
        );
    }
}

module.exports = Luske;
